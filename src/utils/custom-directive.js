import Vue from "vue";

Vue.directive('theme', {
    bind: function (el, binding) {
        if (binding.value == 'light') {
            el.style.backgroundColor = "white";
        }
        else if (binding.value == 'dark') {
            el.style.backgroundColor = "rgb(36, 36, 36)";
            el.style.color = "white";
        }
    },
    update: function (newValue, oldValue) {
        if (oldValue.value == 'light') {
            newValue.style.background = "white";
            newValue.style.color = "black";
        }
        else if (oldValue.value == 'dark') {
            newValue.style.background = "rgb(36, 36, 36)";
            newValue.style.color = "white";
        }
    }
})

Vue.directive("font-size", (el, binding) => {
    var defaultSize;
    if (binding.modifiers.small) {
        defaultSize = 12;
    } else if (binding.modifiers.large) {
        defaultSize = 32;
    } else {
        defaultSize = 16;
    }

    el.style.fontSize = defaultSize + "px";
})

Vue.directive("font-color", (el, binding) => {
    var color = "ff0000";
    switch (binding.arg) {
        case "Aquamarine":
            color = "7FFFD4";
            break;
        case "DarkOrange":
            color = "FF8C00";
            break;
        default:
            color = "ff0000";
            break;
    }
    el.style.color = "#" + color;
})