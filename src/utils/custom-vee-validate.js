import Vue from "vue";
import { required, email } from "vee-validate/dist/rules";
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate/dist/vee-validate.full.esm';
import axios from "axios"

Vue.component('ValidationProvider', ValidationProvider);

Vue.component('ValidationObserver', ValidationObserver);

//Custom validate
extend('requiredEmail', {
    ...required,
    message: 'Email is required. Please enter your email'
});

extend('formatEmail', {
    ...email,
    message: 'Email must be valid'
});

//Async validation
extend('isExist', {
    validate: async value => {
        let isFail = null;
        await axios.get("http://jsonplaceholder.typicode.com/users").then(json => {
            isFail = json.data.some((val) => val.email == value);
        });
        return !isFail;
    },
    message: 'A user with that email already exists!'
});