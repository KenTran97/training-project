import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import "ant-design-vue/dist/antd.css";
import Antd from "ant-design-vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueMoment from "vue-moment";
import Vuelidate from 'vuelidate'
import "./utils/custom-vee-validate";
import "./utils/custom-directive";

Vue.use(Vuelidate);

Vue.use(VueMoment);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Antd);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
