import axios from "axios";
import store from "../store/index";

const HTTP = axios.create({
    baseURL: "http://jsonplaceholder.typicode.com/",
});

HTTP.interceptors.request.use(function (config) {
    store.commit("loading", true);
    return config;
}, function (error) {
    store.commit("loading", false);
    return Promise.reject(error);
});

HTTP.interceptors.response.use(function (response) {
    store.commit("loading", false);
    return response;
}, function (error) {
    store.commit("loading", false);
    return Promise.reject(error);
});

export default HTTP;
